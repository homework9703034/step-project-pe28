"use strict";

// variable is main storage and will be modified all time
let DATA = [
    {
        id: 1,
        visible: true,
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m1.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "8 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        id: 2,
        visible: true,
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f1.png",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        id: 3,
        visible: true,
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m2.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        id: 4,
        visible: true,
        "first name": "Тетяна",
        "last name": "Мороз",
        photo: "./img/trainers/trainer-f2.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
    },
    {
        id: 5,
        visible: true,
        "first name": "Сергій",
        "last name": "Коваленко",
        photo: "./img/trainers/trainer-m3.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
    },
    {
        id: 6,
        visible: true,
        "first name": "Олена",
        "last name": "Лисенко",
        photo: "./img/trainers/trainer-f3.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
    },
    {
        id: 7,
        visible: true,
        "first name": "Андрій",
        "last name": "Волков",
        photo: "./img/trainers/trainer-m4.jpg",
        specialization: "Бійцівський клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
    },
    {
        id: 8,
        "first name": "Наталія",
        "last name": "Романенко",
        photo: "./img/trainers/trainer-f4.jpg",
        specialization: "Дитячий клуб",
        category: "спеціаліст",
        experience: "3 роки",
        description:
            "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
    },
    {
        id: 9,
        visible: true,
        "first name": "Віталій",
        "last name": "Козлов",
        photo: "./img/trainers/trainer-m5.jpg",
        specialization: "Тренажерний зал",
        category: "майстер",
        experience: "10 років",
        description:
            "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
    },
    {
        id: 10,
        visible: true,
        "first name": "Юлія",
        "last name": "Кравченко",
        photo: "./img/trainers/trainer-f5.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
    },
    {
        id: 11,
        visible: true,
        "first name": "Олег",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-m6.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "12 років",
        description:
            "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
    },
    {
        id: 12,
        visible: true,
        "first name": "Лідія",
        "last name": "Попова",
        photo: "./img/trainers/trainer-f6.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
    },
    {
        id: 13,
        visible: true,
        "first name": "Роман",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m7.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
    },
    {
        id: 14,
        visible: true,
        "first name": "Анастасія",
        "last name": "Гончарова",
        photo: "./img/trainers/trainer-f7.jpg",
        specialization: "Басейн",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
    },
    {
        id: 15,
        visible: true,
        "first name": "Валентин",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-m8.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
    },
    {
        id: 16,
        visible: true,
        "first name": "Лариса",
        "last name": "Петренко",
        photo: "./img/trainers/trainer-f8.jpg",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "7 років",
        description:
            "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
    },
    {
        id: 17,
        visible: true,
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m9.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "11 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        id: 18,
        visible: true,
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f9.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        id: 19,
        visible: true,
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m10.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        id: 20,
        visible: true,
        "first name": "Наталія",
        "last name": "Бондаренко",
        photo: "./img/trainers/trainer-f10.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "8 років",
        description:
            "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
    },
    {
        id: 21,
        visible: true,
        "first name": "Андрій",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m11.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
    },
    {
        id: 22,
        visible: true,
        "first name": "Софія",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-f11.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "6 років",
        description:
            "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
    },
    {
        id: 23,
        visible: true,
        "first name": "Дмитро",
        "last name": "Ковальчук",
        photo: "./img/trainers/trainer-m12.png",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
    },
    {
        id: 24,
        visible: true,
        "first name": "Олена",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-f12.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "5 років",
        description:
            "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
    },
];

/**
 *
 * @param { String } string
 *
 * @returns { HTMLElement }
 */
const convertStringToHTML = (string) => {
    const parser = new DOMParser();
    const html = parser.parseFromString(string, "text/html");

    return html.body.firstChild;
}

/**
 * Update overflow for body
 *
 * @param { String } value
 */
const ToggleBodyOverflow = (value) => {
    document.body.style.overflow = value;
}

/**
 * Sort trainers by key
 *
 * @param { String } key - default | lastName | experience
 *
 * @returns { Array }
 */
const TrainersSorting = (key) => {
    const sortings = {
        default: () => {
            DATA = DATA.sort((a, b) => a.id - b.id);
        },
        lastName: () => {
            const comparator = (a, b) => {
                const first = a["last name"].toLowerCase();
                const second = b["last name"].toLowerCase();

                return first.localeCompare(second);
            }

			DATA = DATA.sort(comparator);
        },
        experience: () => {
            const comparator = (a, b) => {
                const first = a.experience.match(/\d+/)[0];
                const second = b.experience.match(/\d+/)[0];

                return first - second;
            }

			DATA = DATA.sort(comparator);
        }
    }

    return sortings[key]();
}

/**
 * Create modal and append to html
 *
 * @param { Object } trainerData
 */
const CreateTrainerModal = (trainerData) => {
    const modal = convertStringToHTML(`
		<div class="modal">
			<div class="modal__body">
				<img
					src="${ trainerData.photo }"
					alt=""
					class="modal__img"
					width="280"
					height="360"
				/>
				<div class="modal__description">
					<p class="modal__name">
						${ trainerData["first name"] } ${ trainerData["last name"] }
					</p>
					<p class="modal__point modal__point--category">
						Категорія: ${ trainerData.category }
					</p>
					<p class="modal__point modal__point--experience">Досвід: ${ trainerData.experience }</p>
					<p class="modal__point modal__point--specialization">
						Напрям тренера: ${ trainerData.specialization }
					</p>
					<p class="modal__text">
						${ trainerData.description }
					</p>
				</div>
			
				<button class="modal__close">
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="30"
						height="30"
						viewBox="0 0 72 72"
					>
						<path d="M 19 15 C 17.977 15 16.951875 15.390875 16.171875 16.171875 C 14.609875 17.733875 14.609875 20.266125 16.171875 21.828125 L 30.34375 36 L 16.171875 50.171875 C 14.609875 51.733875 14.609875 54.266125 16.171875 55.828125 C 16.951875 56.608125 17.977 57 19 57 C 20.023 57 21.048125 56.609125 21.828125 55.828125 L 36 41.65625 L 50.171875 55.828125 C 51.731875 57.390125 54.267125 57.390125 55.828125 55.828125 C 57.391125 54.265125 57.391125 51.734875 55.828125 50.171875 L 41.65625 36 L 55.828125 21.828125 C 57.390125 20.266125 57.390125 17.733875 55.828125 16.171875 C 54.268125 14.610875 51.731875 14.609875 50.171875 16.171875 L 36 30.34375 L 21.828125 16.171875 C 21.048125 15.391875 20.023 15 19 15 z"></path>
					</svg>
				</button>
			</div>
		</div>
	`)

    const button = modal.querySelector(".modal__close");
    button.addEventListener("click", () => {
        modal.remove();
        ToggleBodyOverflow("auto");
    })

    ToggleBodyOverflow("hidden");
    document.body.appendChild(modal);
}

/**
 * Build html structure with trainer info
 *
 * @returns { HTMLElement }
 */
const BuildTrainerCard = (trainerData) => {
    const card = convertStringToHTML(
        `
			<li class="trainer">
				<img
					src="${ trainerData.photo }"
					alt=""
					class="trainer__img"
					width="280"
					height="300"
				/>
				<p class="trainer__name">
					${ trainerData["first name"] } ${ trainerData["last name"] }
				</p>
				<button class="trainer__show-more" type="button">
					ПОКАЗАТИ
				</button>
			</li>
		`
    )

    const button = card.querySelector(".trainer__show-more");
    button.addEventListener("click", () => {
        CreateTrainerModal(trainerData)
    })

    return card;
}

/**
 * Initialization trainer cards
 *
 * @param { Array } trainers
 */
const RenderTrainerCards = () => {
    const trainersContainer = document.querySelector(".trainers-cards__container");
    if(!trainersContainer) {
        return;
    }

    trainersContainer.innerHTML = "";
    for(let i = 0; i < DATA.length; i++) {
        if(!DATA[i].visible) {
            continue;
        }

        trainersContainer.appendChild(BuildTrainerCard(DATA[i]));
    }
}

const InitSorting = () => {
    const root = document.querySelector(".sorting");
    if(!root) {
        return;
    }
    root.removeAttribute("hidden");
    const buttons = root.querySelectorAll("button.sorting__element");

    // click handler
    const handler = (event) => {
        const element = event.target;
        if(!element) {
            return;
        }

        const isActive = element.classList.contains("sorting__btn--active");
        if(isActive) {
            return;
        }

        // reset active
        for(let i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove("sorting__btn--active");
        }

        element.classList.add("sorting__btn--active");

		TrainersSorting(element.dataset.sorting);
        RenderTrainerCards();
    }

    for(let i = 0; i < buttons.length; i++) {
        const button = buttons[i];
        button.addEventListener("click", handler)
    }
}

const InitFilter = () => {
    const sideBar = document.querySelector(".sidebar");
    sideBar.removeAttribute("hidden");

    const form = document.querySelector(".sidebar__filters.filters");
    if(!form) {
        return;
    }

    const categoryMapping = new Map(
        [
            [ "all", null ],
            [ "gym", "Тренажерний зал" ],
            [ "fight club", "Бійцівський клуб" ],
            [ "kids club", "Дитячий клуб" ],
            [ "swimming pool", "Басейн" ],
        ]
    )

    const specializationsMapping = new Map(
        [
            [ "all", null ],
            [ "master", "майстер" ],
            [ "specialist", "спеціаліст" ],
            [ "instructor", "інструктор" ],
        ]
    )

    form.addEventListener("submit", (event) => {
        event.preventDefault();

        const inputs = event.target.querySelectorAll("input.filters__input[type=radio]");

        const filter = {
            specialization: null,
            category: null
        }

        for(let i = 0; i < inputs.length; i++) {
            const input = inputs[i];
            if(!input.checked) {
                continue;
            }

            if(input.name === "direction") {
                filter.specialization = categoryMapping.get(input.value);
            }

            if(input.name === "category") {
                filter.category = specializationsMapping.get(input.value);
            }
        }

        // reset visiblity
        for(let i = 0; i < DATA.length; i++) {
            DATA[i].visible = true;
        }

		if(filter.specialization) {
            for(let i = 0; i < DATA.length; i++) {
                if(filter.specialization === DATA[i].specialization) {
                    continue;
                }

                DATA[i].visible = false;
            }
		}

        if(filter.category) {
            for(let i = 0; i < DATA.length; i++) {
                if(filter.category === DATA[i].category) {
                    continue;
                }

                DATA[i].visible = false;
            }
        }

        RenderTrainerCards();
    })
}

document.addEventListener("DOMContentLoaded", () => {
    RenderTrainerCards();
    InitSorting();
    InitFilter();
})

